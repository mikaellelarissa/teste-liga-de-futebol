package main;

import application.JogadorAplicacao;
import application.TimeAplicacao;
import java.util.Scanner;

public class Aplica��o {

	static Scanner sc = new Scanner(System.in);

	public static void exibirCadastramento() {
		Integer operacao;
		
		char escolha = 's';
		while (escolha == 's') {
			System.out.println("#CADASTRO# \n(1) Time \n(2) Jogador \n(3) Consultar Jogadores do Time \n(4) Fechar");
			operacao = sc.nextInt();
			if (operacao == 1) {
				TimeAplicacao.cadastrarTime();
			} else if (operacao == 2) {
				JogadorAplicacao.cadastrarJogador();
			} else if (operacao == 3) {
				TimeAplicacao.consultarJogadores();
			} else if (operacao == 4) {
			} else {
			}
			
			System.out.println("Deseja realizar outra opera��o? \n(s) sim   (n) n�o");
			escolha = sc.nextLine().charAt(0);
		}

	}

}
