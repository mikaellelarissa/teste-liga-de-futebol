package service;

import java.util.ArrayList;

public class Time {
	
	public String nome;
	
	public String codigo;
	
	public static ArrayList<String> listaJogadores = new ArrayList<>();
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public static ArrayList<String> getJogadores() {
		return listaJogadores;
	}
	
}
