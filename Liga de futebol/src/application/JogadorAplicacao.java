package application;

import java.util.Scanner;
import service.Jogador;
import entitie.CadastrarJogador;

public class JogadorAplicacao {

	static Scanner sc = new Scanner(System.in);
	
	public static void cadastrarJogador() {

		char escolha = 's';
		while (escolha == 's') {
			System.out.println("CADASTRO DO JOGADOR");

			String nome;
			String numeroCamisa;
			String titularidade;

			System.out.println("INFORME OS DADOS DO JOGADOR");

			System.out.println("Nome: ");
			nome = sc.nextLine();

			System.out.println("Nmero da camisa: ");
			numeroCamisa = sc.nextLine();

			System.out.println("Titular ou reserva: \n(t) Titular (r) Reserva");
			titularidade = sc.nextLine();

			Jogador jogador = new Jogador();
			jogador.nome = nome;
			jogador.numeroCamisa = numeroCamisa;
			jogador.titularidade = titularidade;

			if (CadastrarJogador.cadastroJogador(jogador)) {
				System.out.println("Cadastro de jogador realizado com sucesso!");
			} else {
				System.out.println("Erro ao tentar realizar cadastro de time!");
			}

			System.out.println("Deseja cadastrar mais um jogador?");
			escolha = sc.nextLine().charAt(0);
		}
	}

}
