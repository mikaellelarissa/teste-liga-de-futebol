package application;

import java.util.ArrayList;
import java.util.Scanner;
import service.Time;
import entitie.CadastrarTime;

public class TimeAplicacao {

	static Scanner sc = new Scanner(System.in);
	static ArrayList<String> listaJogadores = new ArrayList<>();

	public static void cadastrarTime() {

		System.out.println("CADASTRO DE TIME");

		String nome;
		String codigo;

		System.out.println("INFORME OS DADOS DO TIME");

		System.out.println("Nome: ");
		nome = sc.nextLine();

		System.out.println("C�digo: ");
		codigo = sc.nextLine();

		Time time = new Time();
		time.nome = nome;
		time.codigo = codigo;

		if (CadastrarTime.cadastroTime(time)) {
			System.out.println("Cadastro de time realizado com sucesso!");
		} else {
			System.out.println("Erro ao tentar realizar cadastro de time!");
		}
		
		char escolha = 's';
		while(escolha == 's') {
			String numeroCamisa;
			String titularidade;

			System.out.println("CADASTRE UM JOGADOR AO TIME");
			System.out.println("Informe o c�digo do time para cadastrar jogador: ");
			codigo = sc.nextLine();
			System.out.println("DADOS DO JOGADOR");
			System.out.println("Nome: ");
			nome = sc.nextLine();
			System.out.println("N�mero da camisa: ");
			numeroCamisa = sc.nextLine();
			System.out.println("Titular ou reserva: \n(t) titular (r) reserva");
			titularidade = sc.nextLine();

			listaJogadores.add(nome);
			listaJogadores.add(numeroCamisa);
			listaJogadores.add(titularidade);
			
			System.out.println("Deseja cadastrar um novo jogador? \n(s) sim   (n) n�o");
			escolha = sc.nextLine().charAt(0);
		}
	}
	
	public static void consultarJogadores() {
		System.out.println(Time.getJogadores());
	}

}
