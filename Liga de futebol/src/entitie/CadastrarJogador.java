package entitie;

import persistence.JogadorPersistence;
import service.Jogador;

public class CadastrarJogador {

	public static Boolean cadastroJogador(Jogador jogador) {
		if (jogador.nome == null || jogador.numeroCamisa == null || jogador.titularidade == null) {
			return false;
		}
		JogadorPersistence.persistir(jogador);
		return true;
	}

}