package entitie;

import persistence.TimePersistence;
import service.Time;

public class CadastrarTime {
	
	public static Boolean cadastroTime(Time time) {
		if (time.nome == null || time.codigo == null) {
			return false;
		}
		TimePersistence.persistir(time);
		return true;
	}

}
