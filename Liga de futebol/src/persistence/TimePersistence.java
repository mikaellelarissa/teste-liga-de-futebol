package persistence;

import service.Time;

public class TimePersistence {

	private static final int QTD_MAX_TIMES = 100;

	static Time[] times = new Time[QTD_MAX_TIMES];
	static int qtd = 0;

	public static void persistir(Time time) {
		times[qtd] = time;
		qtd++;
	}

}
