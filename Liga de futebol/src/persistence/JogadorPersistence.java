package persistence;

import service.Jogador;

public class JogadorPersistence {

	private static final int QTD_MAX_JOGADORES = 1100;

	static Jogador[] jogadores = new Jogador[QTD_MAX_JOGADORES];
	static int qtd = 0;

	public static void persistir(Jogador jogador) {
		jogadores[qtd] = jogador;
		qtd++;
	}

}
